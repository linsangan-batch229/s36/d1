//functions

const { request } = require("express");
const Task = require("../models/task.js");

//get all tasks
module.exports.getAllTasks = () => {
    //find all data in DB
    return Task.find({}).then(results => {
        return results;
    }) 
}


//create a new tasks
module.exports.createTask = (requestBody) => {
    //hold the data first
    //call the model 'Task'
    let newTask = new Task({
        name: requestBody.name
    });

    return newTask.save().then((task, error) => {
        if (error){
            console.log(error);
            return false;
        } else {
            return task;
        }
    })
} 


//delete task
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if(error) {
            console.log(error);
            return false;
        } else {
            return removedTask;
        }
    })
}


//update task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error){
            console.log(error);
            return false;
        }
        
        //update
        result.name = newContent.name;

        return result.save().then((updatedTask, error) => {
            if (error) {
                console.log(error);
            } else {
                return updatedTask;
            }
        })

    })
}


//SELECT SPECIFIC TASK
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return result;
        }
    })
}


//UPDATE TASK TO 'COMPLETE
module.exports.completeTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
        }

        //set only the data
        result.status = "complete";

        //save to the DB
        return result.save().then((updatedTask, error) => {
            if(error) {
                console.log(error);
                return false;
            } else {
                return updatedTask;
            }
        })
    })
}