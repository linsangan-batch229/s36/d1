//SCHEMA and MODELS 
//need si mongoDB

const mongoose = require('mongoose');

//blueprint of data
//SCHEMA
const taskSchema =  new mongoose.Schema({
    name: {
        type: String,
        require : [true, "Task name is required"]
    },

    status : {
        type: String,
        default : "pending"
    }
});

//export si Model sa ibang file
// 'Task' self provid
module.exports = mongoose.model('Task', taskSchema);
