//import express lib
const express = require('express');
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js");

// [SECTION] - Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

// GET METHOD
// Router to get all task
router.get("/", (req,res) => {
    taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController)); 
});


//Creating a new tasks [POST]
router.post("/", (req, res) => {
    taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
})




//DELETE TASK [DELETE]
//wildcard - id
router.delete("/:id", (req,res) => {
    taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});



//[UPDATE] task
router.put("/:id", (req, res) => {
    taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//[SPECIFIC] task
router.get("/:id", (req, res) => {
    taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


//[UPDATE] status to 'complete'
router.put("/:id/complete", (req, res) => {
    taskControllers.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})



// [EXPORT] 
// export - nag lalabas
// pwede na gamitin yung router sa ibang files
module.exports = router;